// dependencies
const express = require('express')
// controller
const { createReview, getReviews } = require('../controllers/reviewControllers')
const router=express.Router()

router.post('/:id', createReview)
router.get('/:id', getReviews)

module.exports = router;