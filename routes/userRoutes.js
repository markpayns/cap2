// dependencies
const express = require('express')
// controller
const {
    createAccount,
    login,
    changePassword,
    updateEmail,
    getAllUsers,
    deleteAccount,
    addToCart,
    createAddress,
    getAddresses,
    setDefaultAddress,
    updateAddress,
    profileDetails
} = require('../controllers/userControllers')
const router=express.Router()
const { verify } = require('../middleware/auth')

// no params
router.post('/signup',createAccount)
router.post('/login',login)
router.post('/changepassword', verify, changePassword);
router.post('/address',createAddress)
router.patch('/update', verify, updateEmail);
router.get('/all', verify, getAllUsers);
router.delete('/delete', deleteAccount);
router.get('/addresses', getAddresses)
router.get('/profile', profileDetails)
// with params
router.post('/add/:productId', addToCart)
router.put('/default/:addressId', setDefaultAddress)
router.put('/update-address/:addressId', updateAddress);


module.exports = router;