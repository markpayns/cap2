const jwt = require('jsonwebtoken');

const createToken = ({ _id, email, isAdmin }) => {
  const data = {
    id: _id,
    email,
    isAdmin,
  };
  return jwt.sign(data, process.env.SECRET);
};

const verify = (req, res, next) => {
  const token = req.headers.authorization;
  if (token) {
    const verified = jwt.verify(token.split(' ')[1], process.env.SECRET);
    if (!verified) {
      res.send('Invalid token');
    } else {
      next();
    }
  } else {
    res.send('No token provided');
  }
};

const decode = (token) => {
  if (!token) return null;
  try {
    return jwt.verify(token.split(' ')[1], process.env.SECRET);
  } catch (err) {
    return null;
  }
};

module.exports = {
  createToken,
  verify,
  decode,
};
