// dependencies
const User = require('../models/User')
const Product = require('../models/Product')
const Cart = require('../models/Cart');
const Address = require('../models/Address');

const { createToken, verify, decode } = require('../middleware/auth')

const bcrypt = require('bcrypt');
const validator = require('validator');
const jwt = require('jsonwebtoken');

// create an account
const createAccount = async (req, res) => {
  const { firstName, lastName, email, password } = req.body;

  // Validate email
  if (!validator.isEmail(email)) {
    return res.status(400).send({ message: 'Invalid email' });
  }
  // Check password strength
  if (!isStrongPassword(password)) {
    return res.status(400).send({ ERROR: 'Password is not strong enough. Include Uppercase, Lowercase, Numbers, and Special Characters' });
  }
  try {
    // Check if email already exists
    const user = await User.findOne({ email });
    if (user) {
      return res.status(400).send({ message: 'Email already exists' });
    }
    // Hash password
    const hashedPassword = await bcrypt.hash(password, 10);
    // Create new user
    const newUser = new User({
      firstName,
      lastName,
      email,
      password: hashedPassword
    });
    const result = await newUser.save();
    res.send({ CONFIRMATION: 'Sign Up Successful'});
  } catch (error) {
    console.log(error);
    res.status(500).send({ ERROR: 'Error creating user' });
  }
};

function isStrongPassword(password) {
  // Check password length
  if (password.length < 10) {
    return false;
  }

  // Check for uppercase, lowercase, numbers, and special characters
  const strongPasswordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{10,})/;
  return strongPasswordRegex.test(password);
}

// log in
const login = async (req, res) => {
  const { email, password } = req.body;

  // Check if password is present
  if (!password) {
    return res.status(400).send({ message: 'Missing password' });
  }

  try {
    // Find user by email
    const user = await User.findOne({ email }).select('+password');
    if (!user) {
      return res.status(404).send({ message: 'User not found' });
    }

    // Check if password is correct
    const passwordCorrect = await bcrypt.compare(password, user.password);
    if (!passwordCorrect) {
      return res.status(401).send({ message: 'Incorrect password' });
    }

    // Retrieve JWT from createToken controller
    const token = await createToken(user);

    res.send({ token});
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'Server error' });
  }
};

const profileDetails = (req, res) =>{
	
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	
	return User.findById(userData.id).then(result => {
		result.password = "Confindential";
		return res.send(result)
	}).catch(err => {
		return res.send(err);
	})
	
}

// change password
const changePassword = async (req, res) => {
  // Destructure the request body
  const { oldPassword, newPassword } = req.body;

  // Get the user's id from the token
  const { id: userId } = decode(req.headers.authorization);

  // Find the user by their id
  const user = await User.findById(userId);

  // Check if the old password is correct
  const passwordCorrect = await bcrypt.compare(oldPassword, user.password);
  if (!passwordCorrect) {
    return res.status(401).send({ message: 'Incorrect password' });
  }

  // Validate the new password
  if (!newPassword || !validator.isLength(newPassword, { min: 10 })) {
    return res.status(400).send({ message: 'Invalid password' });
  }

  // Hash the new password
  const hashedPassword = await bcrypt.hash(newPassword, 10);

  // Update the user's password
  user.password = hashedPassword;
  await user.save();

  // Send a success message
  res.send({ message: 'Password changed successfully' });
};

// change email
const updateEmail = async (req, res) => {
  const { email } = req.body;

  // Check if email is present and valid
  if (!email || !validator.isEmail(email)) {
    return res.status(400).send({ message: 'Invalid email' });
  }

  // Check if user is logged in
  const token = req.headers.authorization;
  if (!token) {
    return res.status(401).send({ message: 'Not logged in' });
  }

  try {
    // Verify and decode JWT
    const decoded = jwt.verify(token.split(' ')[1], process.env.SECRET);
    if (!decoded || !decoded.id) {
      return res.status(401).send({ message: 'Invalid token' });
    }

    // Find user by id
    const user = await User.findById(decoded.id);
    if (!user) {
      return res.status(404).send({ message: 'User not found' });
    }

    // Update email
    user.email = email;
    await user.save();

    res.send({ message: 'Email updated' });
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'Server error' });
  }
};

// retrieve all users
const getAllUsers = async (req, res) => {
  const userData=decode(req.headers.authorization)
  // Check if user is an admin
  if (!userData.isAdmin) {
    return res.status(401).send({ message: 'Unauthorized' });
  }

  try {
    // Find all users
    const users = await User.find({});
    res.send(users);
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'Server error' });
  }
};

// delete own account
const deleteAccount = async (req, res) => {
  try {
    // Verify JWT
    await verify(req, res);

    // Get user ID from JWT
    const { userId } = decode(req.headers.authorization);

    // Find user in database
    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(404).send({ message: 'User not found' });
    }

    // Delete user from database
    await User.deleteOne({ _id: userId });

    res.send({ message: 'Account deleted successfully' });
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'Server error' });
  }
};

// create address
const createAddress = async (req, res) => {
  const userData = decode(req.headers.authorization);
  const { address, city, province, zip } = req.body;

  // Create a new address and save it
  const newAddress = new Address({
    user: userData.id,
    address,
    city,
    province,
    zip
  });
  await newAddress.save();

  // Find the user and update their addresses
  const user = await User.findById(userData.id);
  user.addresses.push(newAddress);
  await user.save();

  return res.send(user);
};

// get all address
const getAddresses = async (req, res) => {
  const userData = decode(req.headers.authorization);

  // Find the user and return their addresses
  const user = await User.findById(userData.id);
  return res.send(user.addresses);
};

// update address
const updateAddress = async (req, res) => {
  try {
    const userData = decode(req.headers.authorization);
    if(!userData.isAdmin){// Get the address _id from the request parameters
      const addressId = req.params.addressId;
      // Find the address by _id
      const Update = await Address.findById(addressId);
      const { address, city, province, zip } = req.body;
      // Update the address fields with the new data from the request body
      Update.address = address;
      Update.city = city;
      Update.province = province;
      Update.zip = zip;
      // Save the updated address
      const updatedAddress = await Update.save();
      // Send the updated address as the response
      res.status(200).send(updatedAddress);
      }else{
        res.status(400).send({err: 'Access Failed'})
      }
  } catch {
    // If an error occurs, send a 500 status code and the error as the response
    res.status(500).send({err: 'Update Failed'});
  }
}

// set address to default
const setDefaultAddress = async (req, res) => {
  try {
    const userData = decode(req.headers.authorization);
    if(!userData.isAdmin){
      // Get the address _id from the request parameters
    const addressId = req.params.addressId;
    // Find the address by _id
    const address = await Address.findById(addressId);
    // Toggle the isDefault field
    address.isDefault = !address.isDefault;
    // Save the updated address
    await address.save();
    // Send the updated address as the response
    res.status(200).send(address);
    }else{
      res.status(401).send({ message: 'Unauthorized' });
    }
  } catch(err) {
    // If an error occurs, send a 500 status code and the error as the response
    res.status(500).send(err);
  }
}

// remove address

// add to cart
const addToCart = async (req, res) => {
  const userData = decode(req.headers.authorization);
  const productId = req.params.productId;
  const { quantity } = req.body;

  // Only non-admin users can add items to their cart
  if (!userData.isAdmin) {
    // Find the product and calculate the subtotal
    const product = await Product.findById(productId);
    const subtotal = quantity * product.price;

    // Create a new cart item and save it
    const newCartItem = new Cart({
      user: userData.id,
      product: product._id,
      quantity,
      subtotal
    });
    await newCartItem.save();
    // Find the user and update their cart
    const user = await User.findById(userData.id);
    user.cart.push(newCartItem);
    await user.save();

    return res.send(user);
  } else {
    res.send('You are an admin');
  }
};

// remove from cart

module.exports = {
    createAccount,
    login,
    changePassword,
    updateEmail,
    getAllUsers,
    deleteAccount,
    addToCart,
    createAddress,
    getAddresses,
    setDefaultAddress,
    updateAddress,
    profileDetails
}